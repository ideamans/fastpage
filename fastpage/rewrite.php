<?php

/**
 * FastPage URL rewriter
 *
 * Licensed under the MIT License
 *
 * @copyright Copyright 2011, ideaman's Inc. (http://www.ideamans.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Starting time.
$FASTPAGE_START = microtime(true) * 1000.0;

// Load FastPage.
include('./lib/fastpage.php');

// Comment in if you want to debug.
// FastPage_Debug::start();

// Boot FastPage rewrite application.
FastPage_App::boot('FastPage_App_Rewrite');
