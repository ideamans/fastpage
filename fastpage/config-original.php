<?php

/**
 * FastPage configuration example
 *
 * Licensed under the MIT License
 *
 * @copyright Copyright 2011, ideaman's Inc. (http://www.ideamans.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Default document root path.
 * Usually the path will be detected from server variables.
 */
// $fastpage->config()->default_document_root = '/var/www/htdocs/';

/**
 * IP addresses to allow debugging.
 */
// $fastpage->config('debug')->allow_ips = array( '192.168.' );

/**
 * Max file size in bytes to convert local image files to Data URI schema.
 */
// $fastpage->config('data_uri')->max_file_size = 1024 * 10; // 10KB.

/**
 * Max redundancy to convert local image files to Data URI schema.
 */
// $fastpage->config('data_uri')->max_redundancy = 3; // Within 3 times.

/**
 * Max file size in bytes to make inline local JavaScript files in HTML source.
 */
// $fastpage->config('inline_js')->max_file_size = 1024 * 10; // 10KB.

/**
 * Probability to run regular tasks.
 */
// $fastpage->config('tasks')->regular_probability = 1000; // 1/1000

/**
 * Server cache frequency.
 */
// $fastpage->config('server_cache')->frequency = 60;

/**
 * Loading plugins.
 * Load plugins usually after configuration.
 */

/**
 * Using APC to cache various data.
 */
// $fastpage->load_plugin('APC');

/**
 * If debug started, load LogFormater plugin.
 */
if ( FastPage_Debug::instance() ) {
  $fastpage->load_plugin('LogFormatter');
}
