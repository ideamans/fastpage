<?php

/**
 * FastPage task runner
 *
 * Licensed under the MIT License
 *
 * @copyright Copyright 2011, ideaman's Inc. (http://www.ideamans.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Starting time.
if ( isset($_GET['start']) ) {
  $FASTPAGE_START = (float)$_GET['start'];
} else {
  $FASTPAGE_START = microtime(true) * 1000.0;
}

// Load FastPage.
include('./lib/fastpage.php');

// Comment in if you want to debug.
// FastPage_Debug::start();

// Boot FastPage task application.
FastPage_App::boot('FastPage_App_Task');
