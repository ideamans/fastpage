<?php

class FastPage_LoggingTest extends UnitTestCase {
  public function testNoLogging() {
    FastPage_Debug::log( null, E_USER_NOTICE, 'NOLOGGING' );
    $this->assertNull( FastPage_Debug::instance() );
  }

  public function testLogging() {
    FastPage_Debug::start();
    $fp = new FastPage;

    $records = FastPage_Debug::instance()->log_records();
    $this->assertEqual( count($records), 0 );

    FastPage_Debug::log( $fp, E_USER_NOTICE, 'NOTICE' );
    FastPage_Debug::log( $fp, E_USER_WARNING, 'WARNING' );
    FastPage_Debug::log( $fp, E_USER_ERROR, 'ERROR', 'HINT' );

    $records = FastPage_Debug::instance()->log_records( E_USER_NOTICE );
    $this->assertEqual( count($records), 1 );

    $records = FastPage_Debug::instance()->log_records( E_USER_WARNING );
    $this->assertEqual( count($records), 1 );

    $records = FastPage_Debug::instance()->log_records( E_USER_ERROR );
    $this->assertEqual( count($records), 1 );

    $records = FastPage_Debug::instance()->log_records( E_USER_WARNING|E_USER_ERROR );
    $this->assertEqual( count($records), 2 );
    $this->assertEqual( $records[0]->message, 'WARNING' );
    $this->assertEqual( $records[0]->level, E_USER_WARNING );
    $this->assertTrue( is_numeric($records[0]->timestamp) );
    $this->assertTrue( $records[0]->timestamp > 0 );
    $this->assertTrue( is_numeric($records[0]->memory_usage) );
    $this->assertTrue( $records[0]->memory_usage > 0 );
    $this->assertEqual( $records[0]->delta_timestamp, 0 );
    $this->assertEqual( $records[0]->delta_memory_usage, 0 );
    $this->assertEqual( $records[1]->message, 'ERROR' );
    $this->assertEqual( $records[1]->level, E_USER_ERROR );
    $this->assertEqual( $records[1]->hint, 'HINT' );
    $this->assertTrue( is_numeric($records[0]->delta_timestamp) );
    $this->assertTrue( $records[1]->delta_timestamp > 0 );
    $this->assertTrue( is_numeric($records[0]->delta_memory_usage) );
  }

  public function _callback_test( $callback, $record, $hint ) {
    // Add field to record.
    $record->test_callback = true;

    return true;
  }

  public function _callback_test_with_hint( $callback, $record, $hint ) {
    // Add field to record.
    $record->test_callback_hint = $hint;

    return true;
  }

  public function testCallback() {
    FastPage_Debug::start();
    $fp = new FastPage;
    $fp->add_callback( 'debug_log', 0, array( $this, '_callback_test' ) );
    $fp->add_callback( 'debug_log', 0, array( $this, '_callback_test_with_hint' ), 'CALLBACK_HINT' );

    FastPage_Debug::log( $fp, E_USER_WARNING, 'NOTICE' );
    $records = FastPage_Debug::instance()->log_records( E_USER_WARNING );
    $this->assertTrue( $records[0]->test_callback );
    $this->assertEqual( $records[0]->test_callback_hint, 'CALLBACK_HINT' );
  }
}
