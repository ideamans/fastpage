<?php

class FastPage_TextFiltersTest extends UnitTestCase {
  public function testTextFilters() {
    $pf = new FastPage_TextFilters;

    // String compare.
    $pf->add('normal.html');
    $this->assertTrue( $pf->match('normal.html') );
    $this->assertFalse( $pf->match('/normal.html') );

    // Ignore case.
    $pf->add( 'ignorecase.html', true );
    $this->assertTrue( $pf->match('Ignorecase.html') );
    $this->assertFalse( $pf->match('/Ignorecase.html') );

    // Wildcard
    $pf->add('*.html');
    $this->assertTrue( $pf->match('wildcard.html') );
    $this->assertTrue( $pf->match('/path/to/wildcard.html') );
    $this->assertFalse( $pf->match('wildcard.css') );

    // Wildcard and ignore case.
    $pf->add( '*.HTML', true );
    $this->assertTrue( $pf->match('/a/b/c.Html') );
    $this->assertFalse( $pf->match('/a/b/c.Css') );

    // Regex.
    $pf = new FastPage_TextFilters;
    $pf->add( '~regex\.html' );
    $this->assertTrue( $pf->match('/regex.html') );
    $this->assertTrue( $pf->match('/path/to/regex.html') );
    $this->assertFalse( $pf->match('/not/match') );

    // Regex and ignore case.
    $pf->add( '~REGEX\.HTML', true );
    $this->assertTrue( $pf->match('/Regex.html') );
    $this->assertTrue( $pf->match('/path/to/Regex.html') );
    $this->assertFalse( $pf->match('/not/match') );
  }
}
