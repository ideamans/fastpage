<?php

class FastPage_ConfigTest extends UnitTestCase {
  public function testHostAndDocumentRoot() {
    $fp = new FastPage_App();

    // Hostname.
    $server_host = $_SERVER['HTTP_HOST'];
    $this->assertEqual( $fp->config()->default_host, $server_host );

    // DocumentRoot
    $document_root = $_SERVER['DOCUMENT_ROOT'];
    $this->assertEqual( $fp->config()->default_document_root, $document_root );

    // DocumentRoot for self.
    $this->assertEqual( $fp->document_root(), $document_root );
    $this->assertEqual( $fp->document_root($fp->host), $document_root );

    // DocumentRoot unset.
    $this->assertNull( $fp->document_root('www.example.com') );

    // DocumentRoot set
    $value = '/var/www/example/com/';
    $fp->document_root( 'www.example.com', $value );
    $this->assertEqual( $fp->document_root('www.example.com'), $value );

    // DocumentRoot unset.
    $this->assertNull( $fp->document_root('example.com') );
  }

  public function testMimeTypes() {
    $fp = new FastPage();

    // Built in.
    $this->assertEqual( $fp->config()->mime_type('jpeg'), 'image/jpeg' );
    $this->assertEqual( $fp->config()->mime_type('jpg'), 'image/jpeg' );

    // Unset.
    $this->assertNull( $fp->mime_type('xxxx') );

    // Set.
    $value = 'text/css';
    $fp->mime_type( 'css', $value );
    $this->assertEqual( $fp->mime_type('css'), $value );
  }

}
