<?php

class FastPage_CacheTest extends UnitTestCase {
  public function cache() {
    $fp = new FastPage;
    $fp->helper_class('cache', 'FastPage_Cache');
    $c = $fp->cache();

    return $c;
  }

  public function testCache() {
    $c = $this->cache();
    $c->flush();

    $this->assertNull( $c->get( 'NS1', 'KEY1' ) );
    $c->set( 'NS1', 'KEY1', 'VALUE1-1' );
    $this->assertEqual( $c->get( 'NS1', 'KEY1' ), 'VALUE1-1' );

    $c->set( 'NS2', 'KEY1', 'VALUE2-1' );
    $this->assertEqual( $c->get( 'NS2', 'KEY1' ), 'VALUE2-1' );

    $c->set( 'NS2', '', 'EMPTY-KEY' );
    $this->assertEqual( $c->get( 'NS2', '' ), 'EMPTY-KEY' );

    $c->set( 'NS2', ' ', 'SHORT-KEY' );
    $this->assertEqual( $c->get( 'NS2', ' ' ), 'SHORT-KEY' );

    $c->set( 'NS2', '.', 'PERIOD-KEY' );
    $this->assertEqual( $c->get( 'NS2', '.' ), 'PERIOD-KEY' );

    $c->set( 'NS2', '..', 'DOUBLE-PERIOD-KEY' );
    $this->assertEqual( $c->get( 'NS2', '..' ), 'DOUBLE-PERIOD-KEY' );

    $c->set( 'NS2', '/path/to', 'PATH-STYLE-KEY' );
    $this->assertEqual( $c->get( 'NS2', '/path/to' ), 'PATH-STYLE-KEY' );

    $c->set( '%', 'KEY1', 'ENCODED-NAMESPACE' );
    $this->assertEqual( $c->get( '%', 'KEY1' ), 'ENCODED-NAMESPACE' );

    if ( !is_a( $this, 'FastPage_Cache_APCTest') ) {
      $c->flush( 'NS1' );
      $this->assertNull( $c->get( 'NS1', 'KEY1' ) );
      $this->assertEqual( $c->get( 'NS2', 'KEY1' ), 'VALUE2-1' );
    }

    $c->flush();
    $this->assertNull( $c->get( 'NS1', 'KEY1' ) );
    $this->assertNull( $c->get( 'NS2', 'KEY1' ) );
  }

  public function testCacheExpiring() {
    if ( !is_a( $this, 'FastPage_Cache_APCTest') ) {
      $c = $this->cache();
      $c->flush();

      $c->set( 'NS1', 'KEY1', 'VALUE1-1', 0 );
      $c->set( 'NS1', 'KEY2', 'VALUE1-2', 10 );
      sleep(1);
      $this->assertNull( $c->get( 'NS1', 'KEY1' ) );
      $this->assertEqual( $c->get( 'NS1', 'KEY2' ), 'VALUE1-2' );

      $c->flush();
    }
  }

  public function testDelete() {
    $c = $this->cache();
    $c->flush();

    $c->set( 'NS1', 'KEY1', 'VALUE1-1', 10 );
    $c->set( 'NS1', 'KEY2', 'VALUE1-2', 10 );
    $c->delete( 'NS1', 'KEY1' );
    $this->assertFalse( $c->exist( 'NS1', 'KEY1' ) );
    $this->assertTrue( $c->exist( 'NS1', 'KEY2' ) );

    $c->flush();
  }

  public function testPurge() {
    if ( !is_a( $this, 'FastPage_Cache_APCTest') ) {
      $c = $this->cache();
      $c->flush();

      $c->set( 'NS1', 'KEY1', 'VALUE1-1', 0 );
      $c->set( 'NS1', 'KEY2', 'VALUE1-2', 10 );
      sleep(1);
      $c->purge();
      $this->assertFalse( $c->exist( 'NS1', 'KEY1' ) );
      $this->assertTrue( $c->exist( 'NS1', 'KEY2' ) );

      $c->flush();
    }
  }
}

class FastPage_FileCacheTest extends FastPage_CacheTest {
  public function cache() {
    $fp = new FastPage;
    $fp->helper_class( 'cache', 'FastPage_FileCache' );
    $c = $fp->cache();

    return $c;
  }

}

class FastPage_Cache_APCTest extends FastPage_CacheTest {
  public function cache() {
    $fp = new FastPage;
    $fp->load_plugin('apc');
    $c = $fp->cache();

    return $c;
  }

}
