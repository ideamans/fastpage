<?php

class FastPage_UtilTest extends UnitTestCase {
  public function testCatPath() {
    $this->assertEqual( FastPage_Util::cat_path('/var/www/html', 'index.html'), '/var/www/html/index.html' );
  }

  public function testMatchIP() {
    $remote = $_SERVER['REMOTE_ADDR'];
    $server = $_SERVER['SERVER_ADDR'];
    $localhost = '127.0.0.1';
    $testing = '192.168.10.1';

    $this->assertTrue( FastPage_Util::match_ips( $testing, $testing, false ) );
    $this->assertFalse( FastPage_Util::match_ips( '192.168.11.1', $testing, false ) );
    $this->assertTrue( FastPage_Util::match_ips( '192.168.', $testing, false ) );
    $this->assertFalse( FastPage_Util::match_ips( '192.168.11.1', $testing, false ) );

    $this->assertTrue( FastPage_Util::match_ips( array($testing, $localhost), $testing, false ) );
    $this->assertTrue( FastPage_Util::match_ips( array('192.168.10', $localhost), $testing, false ) );
    $this->assertTrue( FastPage_Util::match_ips( array('192.168.10', $localhost), $localhost, false ) );
    $this->assertFalse( FastPage_Util::match_ips( array('192.168.11', $localhost), $testing, false ) );

    $this->assertTrue( FastPage_Util::match_ips( $testing, $server ) );
    $this->assertTrue( FastPage_Util::match_ips( $testing, $localhost ) );
    $this->assertFalse( FastPage_Util::match_ips( $testing, '192.168.11.1' ) );
    $this->assertTrue( FastPage_Util::match_ips( array(), $server ) );
    $this->assertTrue( FastPage_Util::match_ips( array(), $localhost ) );
  }
}
